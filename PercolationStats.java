import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.StdOut;

public class PercolationStats {
    private Percolation percolation;
    private int trials;
    private double [] trialResults;;
    // perform trials independent experiments on an n-by-n grid
    public PercolationStats(int n, int trials){
        if(n <= 0 || trials <= 0)
            throw new java.lang.IllegalArgumentException();
//        StdRandom.setSeed(Long.parseLong(1316600616575));
        this.trials = trials;
        trialResults = new double [trials];
        int row, col, count = 0;
        while(count < trials){
            double threshold = 0;
            percolation = new Percolation(n);
            while(!percolation.percolates()){
                row = StdRandom.uniform(1, n + 1);
                col = StdRandom.uniform(1, n + 1);
                
                while(percolation.isOpen(row, col)){
                    row = StdRandom.uniform(1, n + 1);
                    col = StdRandom.uniform(1, n + 1);
                }
                percolation.open(row, col);
                threshold++;
            }
            trialResults[count] = threshold/(n * n);
            count++;
        }
    }
    
    // sample mean of percolation threshold
    public double mean(){
        return StdStats.mean(trialResults);
    }
    
    // sample standard deviation of percolation threshold
    public double stddev(){
        return StdStats.stddev(trialResults);
    }
    
    // low  endpoint of 95% confidence interval
    public double confidenceLo(){
        return (mean() - ((1.96 * StdStats.var(trialResults))/Math.sqrt(trials)));
    }
    
    // high endpoint of 95% confidence interval
    public double confidenceHi(){
        return (mean() + ((1.96 * StdStats.var(trialResults))/Math.sqrt(trials)));
    }

    // test client (described below)
    public static void main(String[] args) {
        int n= Integer.parseInt(args[0]);
        int T= Integer.parseInt(args[1]);
        PercolationStats percolationStats = new PercolationStats(n, T);
        StdOut.println("mean                    ="+percolationStats.mean());
        StdOut.println("stddev                  ="+percolationStats.stddev());
        StdOut.println("95% confidence interval = ["+percolationStats.confidenceLo()+", "+percolationStats.confidenceHi()+"]");
    }
}