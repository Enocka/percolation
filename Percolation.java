import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import edu.princeton.cs.algs4.StdOut;

import java.lang.IllegalArgumentException;

public class Percolation {
    private WeightedQuickUnionUF grid;
    private int [] openedStatus;
    private int size, length, percolationPoint;
    private int openCount = 0;
    private int collapseCordinates(int x, int y){
        return (((x - 1) * length) + y - 1) + length;
    }
    private int validateIndices(int x, int y){
        int index = collapseCordinates(x, y);
        if (index < 0 ) {
            throw new IllegalArgumentException();
        }
        if (index > size) {
            throw new IllegalArgumentException();
        }
        return index;
    }
    // create n-by-n grid, with all sites blocked
    public Percolation(int n) {
        length = n;
        size = (n + 2) * (n + 2);
        openedStatus  = new int [size];
        openedStatus[0] = 0;
        openedStatus[size - 1] = 0;
        grid = new WeightedQuickUnionUF(size);
    }

    // open site (row, col) if it is not open already
    public void open(int row, int col) {
        int index = validateIndices(row, col);
        openedStatus[index] = 1;
        openCount++;
        int topCell, bottomCell;
        if (row == 1) {
            topCell = 0;
            grid.union(index, topCell);
        } else {
            topCell = collapseCordinates((row - 1), col);
            if (isOpen(row - 1, col)) grid.union(index, topCell);
        }
        if (row == length) {
            bottomCell = collapseCordinates(length, length) + 1;
            grid.union(index, bottomCell);
        } else {
            bottomCell = collapseCordinates((row + 1), col);
            if (isOpen(row + 1, col)) grid.union(index, bottomCell);
        }
        if (col == 1){
            if (isOpen(row, col + 1)) grid.union(index, index + 1);
        }else if (col == length){
            if (isOpen(row, col - 1)) grid.union(index, index - 1);
        } else {
            if (isOpen(row, col - 1)) grid.union(index, index - 1);
            if (isOpen(row, col + 1)) grid.union(index, index + 1);
        }
    }

    // is site (row, col) open?
    public boolean isOpen(int row, int col) {
        int index = validateIndices(row, col);
        if(openedStatus[index] == 1) return true;
        return false;
    }

    // is site (row, col) full?
    public boolean isFull(int row, int col) {
        int index = collapseCordinates(row, col);
        return grid.connected(0, index);
    }

    public int numberOfOpenSites() {
        return openCount;
    }

    public boolean percolates() {
        if (grid.connected(0, collapseCordinates(length, length) + 1)) percolationPoint = openCount;
        return grid.connected(0, collapseCordinates(length, length) + 1);
    }

    public static void main(String[] args) {

    }
}
